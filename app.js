var express = require('express');
var app = express();

app.get('/', function(req,res) {
    res.send('Hi there, welcome to my routes exercise!');
    console.log('home');
});

app.get('/speak/:animal', function(req,res) {
    var an = req.params.animal.toLowerCase();
    var sounds = {
        pig: 'Oink',
        cow: 'Moo',
        dog: 'Woof Woof!'
    }
    var sound = sounds[an];
    if (sound !== undefined) {
        res.send('The ' + an + ' goes ' + sound);
    }
    else {
        
    }
    // switch (an) {
    //     case 'pig':
    //         res.send('The '+ an + ' goes \'Oink\'!');
    //         break;
    //     case 'cow':
    //         res.send('The '+ an + ' goes \'Moo\'!');
    //         break;
    //     case 'dog':
    //         res.send('The '+ an + ' goes \'Woof Woof\'!');
    //         break;
    
    //     default:
    //         break;
    // }
});
app.get('/repeat/:word/:count', function(req,res) {
    var word = req.params.word;
    var count = req.params.count;
    var result = '';
    for (let i = 0; i < count; i++) {
        result += word + ' ';
    }
    res.send(result);
});


app.get('*', function(req,res) {
    res.send('sorry page not found.');
    console.log('no page at *');
});


app.listen(3000, function(){
    console.log('route-exercise has started!');
});